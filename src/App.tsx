import React from 'react';
import './App.css';
import { StudentsRouter } from './routes';

function App() {
  return (
    <StudentsRouter />
  );
}

export default App;
