import React from 'react';

import { useLayoutResizing } from '../../../../hooks';
import { ContentContainer } from './content-container';
import { DesktopNavbar } from './desktop-navbar';
import { MobileBottomBar } from './mobile-bottom-bar';
import { MobileTopNavbar } from './mobile-top-navbar';

interface AdaptabeLayoutProps {
  children?: React.ReactNode
}

export function AdaptabeLayout(props: AdaptabeLayoutProps) {
  const { desktopMode } = useLayoutResizing();

  return (
    <div>
      {desktopMode ? <DesktopNavbar defaultOptionName="home" /> : <MobileTopNavbar />}
      <ContentContainer>
        <div>
          <div className="left_column">
            left column
          </div>
          <div className="right_column">
            {props.children}
          </div>
        </div>
      </ContentContainer>
      {!desktopMode && <MobileBottomBar />}
    </div>
  );
}
