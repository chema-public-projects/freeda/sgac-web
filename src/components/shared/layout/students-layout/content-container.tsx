import React from 'react';

import './content-container.css';

interface ContentContainerProps {
  children?: React.ReactNode
}

export function ContentContainer(props: ContentContainerProps) {
  return (
    <div className="students-content-container">
      {props.children}
    </div>
  );
}
