import React, { useState } from 'react';
import './desktop-navbar-styles.css';
import { MenuItem } from './menu-item';

interface DesktopNavbarProps {
  defaultOptionName: string;
}

export function DesktopNavbar({ defaultOptionName }: DesktopNavbarProps) {
  const [selectedMenuItem, setSelectedMenuItem] = useState<string>(defaultOptionName);

  function onSelectMenuItem(selectedItem: string) {
    setSelectedMenuItem(selectedItem);
  }

  return (
    <div className="students-desktop-navbar">
      <ul className="menu-items">
        <MenuItem
          optionName="home"
          selectedOptionName={selectedMenuItem}
          onSelect={(optionName) => onSelectMenuItem(optionName)}>
          SGAC
        </MenuItem>
        <MenuItem
          optionName="activities"
          selectedOptionName={selectedMenuItem}
          onSelect={(optionName) => onSelectMenuItem(optionName)}>
          🚴 Mis actividades
        </MenuItem>
        <MenuItem
          optionName="explore"
          selectedOptionName={selectedMenuItem}
          onSelect={(optionName) => onSelectMenuItem(optionName)}>
          🔎 Explorar
        </MenuItem>
        <MenuItem
          optionName="announcements"
          selectedOptionName={selectedMenuItem}
          onSelect={(optionName) => onSelectMenuItem(optionName)}>
          🔔 Avisos
        </MenuItem>
      </ul>
      <div className="user-menu">
        <div>🙂 Chema CLi</div>
      </div>
    </div>
  );
}
