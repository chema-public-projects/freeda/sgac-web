import React from 'react';

interface MenuItemProps {
  children?: React.ReactNode
  selectedOptionName: string
  optionName: string
  onSelect: (optionName: string) => void
}

export function MenuItem(props: MenuItemProps) {
  return (
    <li
      className={(props.optionName === props.selectedOptionName && 'selected') || ''}
      onClick={() => props.onSelect(props.optionName)}>
      {props.children}
    </li>
  );
}
