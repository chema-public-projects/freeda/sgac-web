import React, { useState } from 'react';
import './mobile-bottom-bar-styles.css';
import { MenuItem } from './menu-item';

export function MobileBottomBar() {
  const [selectedMenuItem, setSelectedMenuItem] = useState<string>('');

  function onSelectMenuItem(selectedItem: string) {
    setSelectedMenuItem(selectedItem);
  }

  return (
    <ul className="students-bottom-navbar">
      <MenuItem
        optionName="activities"
        selectedOptionName={selectedMenuItem}
        onSelect={(optionName) => onSelectMenuItem(optionName)}>
        <span className="icon">🚴</span>
        <span className="label">Actividades</span>
      </MenuItem>
      <MenuItem
        optionName="explore"
        selectedOptionName={selectedMenuItem}
        onSelect={(optionName) => onSelectMenuItem(optionName)}>
        <span className="icon">🔎</span>
        <span className="label">Explorar</span>
      </MenuItem>
      <MenuItem
        optionName="announcements"
        selectedOptionName={selectedMenuItem}
        onSelect={(optionName) => onSelectMenuItem(optionName)}>
        <span className="icon">🔔</span>
        <span className="label">Avisos</span>
      </MenuItem>
    </ul>
  );
}
