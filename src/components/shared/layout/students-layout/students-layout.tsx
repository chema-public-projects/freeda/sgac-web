import React from 'react';
import { AdaptabeLayout } from './adaptabe-layout';

interface StudentsLayoutProps {
  children?: React.ReactNode
}

export function StudentsLayout(props: StudentsLayoutProps) {
  return (
    <AdaptabeLayout>
      {props.children}
    </AdaptabeLayout>
  );
}
