import { useEffect, useState } from 'react';
import { WindowSize } from '../types';

interface UseLayoutResizingState {
  windowWidth: number;
  windowHeight: number;
  windowSize: WindowSize;
}

/**
 * Returns info about the resize and size events and values
 * @returns 
 */
export function useLayoutResizing() {
  const [state, setState] = useState<UseLayoutResizingState>({
    windowWidth: 0,
    windowHeight: 0,
    windowSize: 'SMALL'
  });

  useEffect(() => {
    function updateDimensions() {
      const windowWidth = typeof window !== 'undefined' ? window.innerWidth : 0;
      const windowHeight = typeof window !== 'undefined' ? window.innerHeight : 0;
  
      const windowSize = getWindowSize(windowWidth ?? 0);
  
      setState({
        windowHeight,
        windowWidth,
        windowSize,
      });
    }

    updateDimensions();
    window.addEventListener('resize', updateDimensions);
    return () => window.removeEventListener('resize', updateDimensions);
  }, []);

  function getWindowSize(windowWidth: number) {
    if (windowWidth <= 500)
      return 'EXTRA_SMALL';
    if (windowWidth <= 768)
      return 'SMALL';
    if (windowWidth <= 1100)
      return 'MEDIUM';
    else
      return 'LARGE';
  }

  // Enables tiny mode for screens smaller than medium size.
  // Medium and Large sizes are expanded
  const sidebarCollapsed = state.windowWidth < 1100;
  const desktopMode = ['MEDIUM', 'LARGE'].includes(state.windowSize);

  return {
    ...state,
    sidebarCollapsed,
    desktopMode
  };
}
