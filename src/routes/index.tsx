import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Dashboard } from '../pages/students';

export function StudentsRouter() {
  return (
    <Router>
      <Route component={Dashboard} path="/students/dashboard" />
    </Router>
  );
}
