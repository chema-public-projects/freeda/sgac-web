export type WindowSize = 'EXTRA_SMALL' | 'SMALL' | 'MEDIUM' | 'LARGE';
